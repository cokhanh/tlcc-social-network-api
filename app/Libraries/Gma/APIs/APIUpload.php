<?php

namespace App\Libraries\Gma\APIs;

use App\Api\Entities\Upload;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Intervention\Image\ImageManagerStatic as Image;

class APIUpload
{

    /**
     * Trả về request để gọi API lấy file
     * params truyền vào bao gồm:
     * - type: là hình ảnh hay file thông thường (image|file)
     * - option: lựa chọn lấy file ảnh đại diện, tài liệu hay khác
     * - user_id: mã người dùng
     */
    public static function getFileToClient($params = [])
    {
        $validate = \Validator::make($params, [
            'type' => [
                'required',
                Rule::in(['image', 'file'])
            ],
            'option' => [
                'required',
                Rule::in(['avatars', 'cover_picture', 'status', 'chat'])
            ],
        ]);

        if ($validate->fails()) {
            return $validate->messages()->toArray();
        }

        $type = $params['type'];
        $option = $params['option'];
        $user_id = $params['user_id'];

        if (($option == 'avatars' || $option == 'cover_picture') && (empty($params['is_status']))) {
            $uploaded = Upload::where([
                'type' => 'image',
                'user_id' => $user_id,
                'option' => $option
            ])->orderBy('created_at', 'desc')->first();
            if (!empty($uploaded)) {
                $uri = env('API_IMG_URL') . "?file=$uploaded->file_name";
                return $uri;
            } else {
                $uri = env('API_IMG_URL') . "?file=default.jpg";
                return $uri;
            }
        }

        if (!empty($params['upload_id'])) {
            $uploaded = Upload::where([
                '_id' => mongo_id($params['upload_id'])
            ])->first();
            
            if (!empty($uploaded)) {
                $uploadItem = [
                    'type' => $uploaded->type
                ];
                if($uploaded->type == 'video'){
                    $uri = env('API_VIDEO_URL') . "?file=$uploaded->file_name";
                    $uploadItem['uri'] = $uri;
                }else{
                    $uri = env('API_IMG_URL') . "?file=$uploaded->file_name";
                    $uploadItem['uri'] = $uri;
                }
                return $uploadItem;
            }
        }

        $uploaded = Upload::where([
            'type' => $type,
            'user_id' => $user_id,
        ])->get();
        $data = [];
        $domain = env('API_IMG_URL');
        if($type == 'video'){
            $domain = env('API_VIDEO_URL');
        }

        if (!empty($uploaded)) {
            foreach ($uploaded as $item) {
                if (strpos($item->path, $option)) {
                    $name = $item->file_name;
                    array_push($data, $domain . "?file=$name");
                    // return $data;
                } else {
                    $uri = $domain . "?file=default.jpg";
                    array_push($data, $uri);
                }
            }
        }
        return $data;
    }

    /**
     * Insert vào collection Upload và lưu vào folder public
     * params truyền vào:
     * - type: là hình ảnh hay file thông thường (image|file)
     * - option: lựa chọn lấy file ảnh đại diện, tài liệu hay khác
     * - user_id: mã nhân viên
     */
    public static function uploadToServer($params = [], $file)
    {
        $isChanged = 0;
        $user_id = $params['user_id'];

        //Kiểm tra type và option
        $validate = \Validator::make($params, [
            // 'type' => [
            //     'required',
            //     Rule::in(['image', 'files','video'])
            // ],
            'option' => [
                'required',
                Rule::in(['avatars', 'cover_picture', 'status', 'chat'])
            ],
        ]);
        
        if ($validate->fails()) {
            return trans('core.invalid_format');
        }

        $option = $params['option'];
        //Kết thúc kiểm tra type và option

        //Xử lý extension
        $file_extension = strtolower(substr(strrchr($file->getClientOriginalName(), "."), 1));
        
        //check if is image
        $imageType = [
            'jpeg',
            'pjpeg',
            'png',
            'x-png',
            'gif',
            'jpg',
            'svg',
            'ico',
            'tiff',
            'gfif',
            'xbm',
            'tif',
            'pjp',
            'svgz',
            'webp',
            'bmp',
            'avif',
            'm4v'
        ];
        if (in_array(strtolower($file_extension), $imageType)) {
            $type = 'image';
        }else{
            $type = 'video';
        }


        /**
         * Lưu file vào path public/...
         */
        $nameRandom = str_random(30);
        $file_name = $nameRandom . '.' . $file_extension;

        //kiểm tra hình ảnh hay file có được đăng bởi user hay chưa.
        //Nếu có rồi thì cập nhật ảnh mới xoá ảnh cũ
        $checkFile = Upload::where([
            'user_id' => $user_id,
            'option' => $option
        ])->first();
        $destination = base_path(). '/public/' . $type;
        $realPath =  '/public/' . $type . '/' . $file_name;
        if (!empty($checkFile)) {
            if ($option != 'chat' && $option != 'status') {
                $isChanged = 1;
            }
        }
        $saveFile = $file->move($destination, $file_name);
        // dd('ok');
        
        // if($file_extension === "mp4"){
        //     $saveFile = $file->move($destination, $file_name);
        // }else{
        //     //resize image
        //     $newImage = Image::make($file)->resize(300,300);
        //     $saveFile = $newImage->save($destination.'/'.$file_name,100,'jpg');
        //     dd('ok');
        // }

        /**
         * Kết thúc lưu file vào path public/...
         */

        /**
         * lưu file vào database
         */

        //update avatar
        if ($isChanged == 1) {
            // $checkFile->file_name = $file_name;
            // $checkFile->path = $realPath;
            // $checkFile->extension = $file_extension;
            // $checkFile->save();
            $uploadParams = [
                'path' => $realPath,
                'user_id' => $user_id,
                'type' => $type,
                'option' => $option,
                'file_name' => $file_name,
                'extension' => $file_extension
            ];


            $createUpload = Upload::create($uploadParams);
            return $createUpload->_id;
        } else {
            $uploadParams = [
                'path' => $realPath,
                'user_id' => $user_id,
                'type' => $type,
                'option' => $option,
                'file_name' => $file_name,
                'extension' => $file_extension
            ];

            $createUpload = Upload::create($uploadParams);
            return $createUpload->_id;
        }
        return;
    }
}
