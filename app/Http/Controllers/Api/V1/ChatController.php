<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Api\Entities\Chat;
use App\Api\Repositories\Contracts\ChatRepository;
use App\Api\Repositories\Contracts\ChatMessagesRepository;
use App\Api\Criteria\ChatCriteria;
use App\Api\Entities\ChatMessages;
use App\Api\Entities\UsersInfo;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    protected $request;
    protected $chatRepository;
    protected $chatMessagesRepository;

    public function __construct(
        Request $request,
        ChatRepository $chatRepository,
        ChatMessagesRepository $chatMessagesRepository
    ) {
        $this->request = $request;
        $this->chatRepository = $chatRepository;
        $this->chatMessagesRepository = $chatMessagesRepository;
    }

    /**
     * api tạo phòng chat
     * route: [POST]api/chat/create-chat-group
     * request url: http://13.76.199.59/api/chat/create-chat-group
     * params:
     *      user_ids: array|required
     *      room_name: required
     * resonse:
     *       {
     *           "error_code": 0,
     *           "message": [
     *               "Successfully"
     *           ],
     *           "data": "Thành công."
     *       }
     */
    public function createChatGroup()
    {
        $validator = \Validator::make($this->request->all(), [
            'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $currentUser = UsersInfo::where([
            '_id' => mongo_id(Auth::getPayLoad()->get('user_id'))
        ])->first();
        if (empty($currentUser)) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }

        $userId = $this->request->get('user_id');
        $userInfo = UsersInfo::where([
            '_id' => mongo_id($userId)
        ])->first();
        if (empty($userInfo)) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }

        $userIds = [
            Auth::getPayLoad()->get('user_id'),
            $userId
        ];
        $room_name = $this->request->get('room_name');

        $chatGroupAttrs = [
            'user_ids' => (array)$userIds
        ];

        $chatGroup = Chat::create($chatGroupAttrs);
        $chatGroup->room_name = $chatGroup->_id;
        $chatGroup->save();
        return $this->successRequest($chatGroup->_id);
    }

    /**
     * api lưu tin nhắn
     * route: [POST]api/chat/save-message
     * request url: http://13.76.199.59/api/chat/save-message
     * params:
     *      chat_group_id: required
     *      content: required
     * header:
     *      Authorization: token
     * resonse:
     *       {
     *           "error_code": 0,
     *           "message": [
     *               "Successfully"
     *           ],
     *           "data": "Thành công."
     *       }
     */
    public function saveMessage()
    {
        $validator = \Validator::make($this->request->all(), [
            'chat_group_id' => 'required',
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $ownerId = Auth::getPayLoad()->get('user_id');
        $chatGroupId = $this->request->get('chat_group_id');
        $content = $this->request->get('content');

        $chatGroup = Chat::where([
            '_id' => mongo_id($chatGroupId)
        ])->first();
        if (empty($chatGroup)) {
            return $this->errorBadRequest(trans('chat.no_exist_chat_group'));
        }

        $chatMessageAttrs = [
            'chat_group_id' => $chatGroupId,
            'owner' => $ownerId,
            'contents' => $content
        ];
        $chatMessage = ChatMessages::create($chatMessageAttrs);
        return $this->successRequest(trans('core.success'));
    }

    /**
     * api lấy danh sách phòng chat
     * route: [POST]api/chat/list-messages
     * request url: http://13.76.199.59/api/chat/list-messages
     * params:
     *      token: required
     * header:
     *      Authorization: token
     * resonse:
     *       {
            "error_code": 0,
            "message": [
                "Successfully"
            ],
            "data": [
                {
                    "id": "5fe0d4f5fa610000f8006902",
                    "room": "truong.phuong.02.03-co.khanh.02.03",
                    "messRecent": "hello",
                    "time": "22/12/2020 12:01:40 AM",
                    "userName": "Nguyễn Trinh",
                    "friend_chat": "Dương Cơ Khánh",
                    "avatar": "http://13.76.199.59/api/get-image?name=Cbqdnlk8mbZfMlukdIKxmW7RKi9ENS.jpg&option=avatars"
                },
                {
                    "id": "5fe1754ef024000036005582",
                    "room": "truong.phuong.02.03-do.tuongkhai.01.09",
                    "messRecent": "hello hello hello hello",
                    "time": "22/12/2020 11:25:50 AM",
                    "userName": "Nguyễn Trinh",
                    "friend_chat": "Đỗ Tường Khải",
                    "avatar": "http://13.76.199.59/api/get-image?name=default.jpg&option=avatars"
                }
            ]
        }
     */
    public function listMessages()
    {
        $validator = \Validator::make($this->request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $userInfo = UsersInfo::where([
            '_id' => mongo_id(Auth::getPayLoad()->get('user_id'))
        ])->first();
        if (empty($userInfo)) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }

        $params = [
            'user_id' => $userInfo->_id
        ];
        $lstMessages = $this->chatRepository->getChatGroup($params);

        $data = [];
        foreach ($lstMessages as $item) {
            $data[] = $item->transform('', $userInfo->_id);
        }

        $dataReverse = array_reverse($data);
        return $this->successRequest($dataReverse);
    }

    /**
     * api lấy tin nhắn của phòng chat
     * route: [POST]api/chat/list-messages-in-group
     * request url: http://13.76.199.59/api/chat/list-messages-in-group
     * params:
     *      chat_group_id: required
     * resonse:
     *       {
            "error_code": 0,
            "message": [
                "Successfully"
            ],
            "data": [
                {
                    "id": "5fe187d5f024000036005585",
                    "contents": "hello",
                    "owner": "do.tuongkhai.01.09",
                    "user_name": "Đỗ Tường Khải"
                },
                {
                    "id": "5fe187ebf024000036005586",
                    "contents": "hello hello hello hello",
                    "owner": "do.tuongkhai.01.09",
                    "user_name": "Đỗ Tường Khải"
                }
            ]
        }
     */
    public function messagesInGroup()
    {
        $validator = \Validator::make($this->request->all(), [
            'chat_group_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $chatGroupId = $this->request->get('chat_group_id');
        $chatGroupInfo = Chat::where([
            '_id' => mongo_id($chatGroupId)
        ])->first();
        if (empty($chatGroupInfo)) {
            return $this->errorBadRequest(trans('chat.no_exist_chat_group'));
        }

        $params = [
            'chat_group_id' => $chatGroupId
        ];
        $chatMessagesByGroup = $this->chatMessagesRepository->getChatMessages($params);

        $data = [];
        foreach ($chatMessagesByGroup as $item) {
            if ($item->contents != '') {
                $data[] = $item->transform();
            }
        }

        return $this->successRequest($data);
    }
}
