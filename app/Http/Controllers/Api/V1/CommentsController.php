<?php

namespace App\Http\Controllers\Api\V1;

use App\Api\Entities\Comments;
use App\Api\Entities\Status;
use App\Api\Entities\Users;
use App\Api\Entities\UsersInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Api\Repositories\Contracts\CommentsRepository;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
    protected $request;
    protected $commentsRepository;

    public function __construct(Request $request, CommentsRepository $commentsRepository)
    {
        $this->request = $request;
        $this->commentsRepository = $commentsRepository;
    }

    public function createComment()
    {
        $validator = \Validator::make($this->request->all(), [
            'status_id' => 'required',
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        
        $user = Auth::user();
        $userInfo = UsersInfo::where([
            '_id' => mongo_id($user->user_id)
        ])->first();
        if (empty($userInfo)) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }

        $content = $this->request->get('content');
        if (empty($content)) {
            return $this->errorBadRequest(trans('comment.invalid_content'));
        }

        $statusId = $this->request->get('status_id');
        $status = Status::where([
            '_id' => mongo_id($statusId)
        ])->first();
        if (empty($status)) {
            return $this->errorBadRequest(trans('status.no_status'));
        }

        $commentParams = [
            'status_id' => $statusId,
            'content'   => $content,
            'user' => Auth::getPayLoad()->get('user_id')
        ];
        $comment = Comments::create($commentParams);
        return $this->successRequest(trans('core.success'));
    }

    public function listComments()
    {
        $validator = \Validator::make($this->request->all(), [
            'status_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $statusId = $this->request->get('status_id');
        $statusInfo = Status::where([
            '_id' => mongo_id($statusId)
        ])->first();
        if (empty($statusInfo)) {
            return $this->errorBadRequest(trans('core.no_status'));
        }

        $data = [];
        $params = [
            'status_id' => $statusId
        ];
        $comments = $this->commentsRepository->getComments($params);

        foreach($comments as $comment){
            $data[] = $comment->transform();
        }

        return $this->successRequest($data);
    }
}
