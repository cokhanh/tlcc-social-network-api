<?php

namespace App\Http\Controllers\Api\V1;

use App\Api\Entities\Status;
use App\Api\Entities\Users;
use App\Api\Entities\UsersInfo;
use App\Api\Repositories\Contracts\StatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Libraries\Gma\APIs\APIUpload;
use Carbon\Carbon;

class StatusController extends Controller
{
    protected $request;
    protected $statusRepository;

    public function __construct(Request $request, StatusRepository $statusRepository)
    {
        $this->request = $request;
        $this->statusRepository = $statusRepository;
    }


    /**
     * api đăng status
     * route: [POST]api/status/upload-status
     * request url: http://api-tlcn.herokuapp.com/api/status/upload-status
     * params:
     *      file: array,
     *      status_setting: required, Rule::in(['priv','pub','friend','other']),
     *      user_tagged_ids: array
     * response:
     *      {
                "error_code": 0,
                "message": [
                    "Successfully"
                ],
                "data": "Thành công."
     *      }
     */
    public function uploadStatus()
    {
        $validator = \Validator::make($this->request->all(), [
            'file' => 'array',
            'user_tagged_ids' => 'array',
            'status_setting' => [
                'required',
                Rule::in([
                    'priv',
                    'pub',
                    'friend',
                    'other'
                ])
            ],
            //'caption'=>'required_without: file:[]'
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $user_id = Auth::getPayLoad()->get('user_id');
        $user_tagged_ids = $this->request->get('user_tagged_ids');
        $status_setting = $this->request->get('status_setting');
        $caption = $this->request->get('caption');

        //check user info
        $userInfo = UsersInfo::where([
            '_id' => mongo_id($user_id)
        ])->first();
        if (empty($userInfo)) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }

        //check user tagged info
        if (isset($user_tagged_ids)) {
            foreach ($user_tagged_ids as $item) {
                $info = UsersInfo::where([
                    '_id' => mongo_id($item)
                ])->first();
                if (empty($info)) {
                    return $this->errorBadRequest(trans('user.account_not_exist'));
                }
            }
        }

        $paramStatus = [
            'user_id' => $user_id,
            'status_setting' => $status_setting,
        ];

        $arrayFiles = $this->request->file('file');
        $paramFileArray = [];
        if (isset($arrayFiles)) {
            //upload file of status
            foreach ($arrayFiles as $file) {
                if (!empty($file)) {
                    $validator = \Validator::make($this->request->all(), [
                        // 'type' => [
                        //     'required',
                        //     Rule::in([0, 1, 2]) //0: image, 1: file, 2: video
                        // ],
                        'option' => [
                            'required',
                            Rule::in([0, 1, 2, 3]) //0: avatars, 1: cover_picture, 2: status, 3: chat
                        ]
                    ]);

                    if ($validator->fails()) {
                        return $this->errorBadRequest($validator->messages()->toArray());
                    }

                    $param = [
                        'user_id' => $user_id,
                        //'type' => $this->request->get('type'),
                        'option' => $this->request->get('option')
                    ];
                    //$type = (int)($this->request->get('type'));
                    $option = (int)($this->request->get('option'));

                    // if ($type == 0) {
                    //     $param['type'] = 'image';
                    // } elseif($type == 1) {
                    //     $param['type'] = 'files';
                    // }else{
                    //     $param['type'] = 'video';
                    // }
                    if ($option == 0) {
                        $param['option'] = 'avatars';
                    } elseif ($option == 1) {
                        $param['option'] = 'cover_picture';
                    } elseif ($option == 2) {
                        $param['option'] = 'status';
                    } else {
                        $param['option'] = 'chat';
                    }
                    $uploadFileId = APIUpload::uploadToServer($param, $file);
                    array_push($paramFileArray, $uploadFileId);
                }
            }
            $paramStatus['upload_ids'] = $paramFileArray;
        }

        if (isset($caption)) {
            $paramStatus['caption'] = $caption;
        }
        if (isset($user_tagged_ids)) {
            $paramStatus['user_tagged_ids'] = $user_tagged_ids;
        }
        $statusUpload = Status::create($paramStatus);
        return $this->successRequest(trans('core.success'));
    }
    /**
     * api cập nhật status
     * route: [POST]api/status/update-status
     * request url: http://api-tlcn.herokuapp.com/api/status/update-status
     * params:
     *      status_id: required
     *      user_tagged_ids: array,
     *      status_setting: required, Rule::in(['priv','pub','friend','other']),
     * response:
     *      {
                "error_code": 0,
                "message": [
                    "Successfully"
                ],
                "data": "Thành công."
     *      }
     */
    public function updateStatus()
    {
        $validator = \Validator::make($this->request->all(), [
            'status_id' => 'required',
            'user_tagged_ids' => 'array',
            'status_setting' => Rule::in([
                'priv',
                'pub',
                'friend',
                'other'
            ])
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $statusId = $this->request->get('status_id');
        //kiểm tra status tồn tại
        $status = Status::where([
            '_id' => mongo_id($statusId)
        ])->first();
        if (empty($status)) {
            return $this->errorBadRequest('core.no_status');
        }

        $newCaption = $this->request->get('new_caption');
        if (isset($newCaption) && $newCaption != $status->caption) {
            if (!empty($newCaption)) {
                $status->caption = $newCaption;
            }
        }

        $userTaggedIds = $this->request->get('user_tagged_ids');
        if (isset($userTaggedIds)) {
            if (isset($status->user_tagged_ids)) {
                $lstIds = (array) $status->user_tagged_ids;
                $newUserTagged = array_merge($lstIds, $userTaggedIds);
                $status->user_tagged_ids = $newUserTagged;
            } else {
                $status->user_tagged_ids = $userTaggedIds;
            }
        }

        $statusSetting = $this->request->get('status_setting');
        if (isset($statusSetting) && $statusSetting != $status->status_setting) {
            $status->status_setting = $statusSetting;
        }

        //cập nhật số like
        $currentUsersLikeArr = [];
        $statusNewLikeAmount = $this->request->get('like');
        $isUnLike = $this->request->get('is_unlike');
        if (isset($statusNewLikeAmount)) {
            $status->like = $statusNewLikeAmount;

            if (isset($isUnLike)) {
                $userUnLike = Auth::getPayLoad()->get('user_id');
                $userLikeArr = $status->user_like;
                for ($i = 0; $i < count($userLikeArr); $i++) {
                    if ($userUnLike == $userLikeArr[$i]) {
                        $userLikeArr[$i] = 'Undefined';
                    }
                }
                $status->user_like = $userLikeArr;
            } else {
                if (isset($status->user_like)) {
                    $userLike = (array)($status->user_like);
                    if(!in_array(Auth::getPayLoad()->get('user_id'),$userLike)){
                        array_push($userLike, Auth::getPayLoad()->get('user_id'));
                        $status->user_like = $userLike;
                    }
                    
                } else {
                    $newUserLike = [];
                    array_push($newUserLike, Auth::getPayLoad()->get('user_id'));
                    $status->user_like = $newUserLike;
                }
            }

            //lấy danh sách người thích bài viết hiện tại
            $currentUserLike = $status->user_like;;
            foreach ($currentUserLike as $item) {
                if ($item != 'Undefined') {
                    $params = [
                        'type' => 'image',
                        'user_id' => $item,
                        'option' => 'avatars'
                    ];
                    $avatarFriendURI = APIUpload::getFileToClient($params);

                    $detailInfo = UsersInfo::where([
                        '_id' => mongo_id($item)
                    ])->first();
                    if (!empty($detailInfo)) {
                        $userName = $detailInfo->last_name . ' ' . $detailInfo->first_name;
                        $userId = $detailInfo->_id;
                    }

                    $userDetail = [
                        'avatar' => $avatarFriendURI,
                        'user_name' => $userName,
                        'user_id' => $userId
                    ];
                    array_push($currentUsersLikeArr, $userDetail);
                }
            }
            $status->save();
            return $this->successRequest($currentUsersLikeArr);
        }

        $status->save();
        return $this->successRequest(trans('core.success'));
    }
    /**
     * api lấy danh sách status cá nhân người dùng
     * route: [POST]api/status/load-personal-status
     * request url: http://api-tlcn.herokuapp.com/api/status/load-personal-status
     * params:
     *      user_id: required
     * response:
     *      {
                "error_code": 0,
                "message": [
                    "Successfully"
                ],
                "data": [
                    {
                        "id": "5fa0fde2982a00001b003be1",
                        "status_setting": "pub",
                        "caption": "Đây là Status đầu tiên",
                        "user_name": "Mỹ Phương"
                    },
                    {
                        "id": "5fa0fdfc982a00001b003be3",
                        "status_setting": "pub",
                        "caption": "",
                        "user_name": "Mỹ Phương"
                    },
                    {
                        "id": "5fa3f6521628000010006b12",
                        "status_setting": "pub",
                        "caption": "test",
                        "user_name": "Mỹ Phương"
                    },
                    {
                        "id": "5fa3f78a1628000010006b14",
                        "status_setting": "pub",
                        "caption": "test update status",
                        "user_name": "Mỹ Phương"
                    },
                    {
                        "id": "5fa40b011628000010006b22",
                        "status_setting": "pub",
                        "caption": "test upload nhiều file 1 lần",
                        "user_name": "Mỹ Phương"
                    }
                ]
     *}
     */
    public function listStatusV1()
    {
        $validator = \Validator::make($this->request->all(), [
            'user_id' => 'required_without:' . 'token',
            'token' => 'required_without:' . 'user_id'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $params = [];
        $userIdNormal = $this->request->get('user_id');
        $userId = '';
        if (isset($userIdNormal)) {
            $userId = $userIdNormal;
            $params['status_setting'] = 'pub';
        } else {
            $userId = Auth::getPayLoad()->get('user_id');
        }

        $params['user_id'] = $userId;
        $getUsersStatuses = $this->statusRepository->getUsersStatuses($params);
        
        $data = [];
        foreach ($getUsersStatuses as $status) {
            $data[] = $status->transform($userId);
        }
        return $this->successRequest(array_reverse($data));
    }
    /**
     * api lấy danh sách status trên news feed
     * route: [POST]api/status/load-personal-status
     * request url: http://api-tlcn.herokuapp.com/api/status/load-personal-status
     * params:
     *      user_id: required
     * response:
     *      {
                    "error_code": 0,
                    "message": [
                        "Successfully"
                    ],
                    "data": [
                        {
                            "id": "5fa0fde2982a00001b003be1",
                            "status_setting": "pub",
                            "caption": "Đây là Status đầu tiên",
                            "user_name": "Mỹ Phương"
                        },
                        {
                            "id": "5fa0fdfc982a00001b003be3",
                            "status_setting": "pub",
                            "caption": "",
                            "user_name": "Mỹ Phương"
                        },
                        {
                            "id": "5fa3f6521628000010006b12",
                            "status_setting": "pub",
                            "caption": "test",
                            "user_name": "Mỹ Phương"
                        },
                        {
                            "id": "5fa3f78a1628000010006b14",
                            "status_setting": "pub",
                            "caption": "test update status",
                            "user_name": "Mỹ Phương"
                        },
                        {
                            "id": "5fa40b011628000010006b22",
                            "status_setting": "pub",
                            "caption": "test upload nhiều file 1 lần",
                            "user_name": "Mỹ Phương"
                        }
                    ]
     *}
     */
    public function listStatusV2()
    {
        $validator = \Validator::make($this->request->all(), [
            'token' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $userId = Auth::getPayLoad()->get('user_id');
        $userInfo = UsersInfo::where([
            '_id' => mongo_id($userId)
        ])->first();
        if (empty($userInfo)) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }

        $params = [
            'user_id' => $userId
        ];

        $data = [];

        $getUsersPersonalStatuses = $this->statusRepository->getUsersStatuses($params);
        $personalStatus = [];
        foreach ($getUsersPersonalStatuses as $status) {
            $personalStatus[] = $status->transform($userId);
        }

        //get public status of user's friend
        $listFriendIds = $userInfo->list_friends;
        $frienrStatus = [];
        if(!empty($listFriendIds)){
            foreach ($listFriendIds as $item) {
                if ($item != 'Undefined') {
                    $param = [
                        'user_id' => $item,
                        'status_setting' => 'pub'
                    ];
                    $getFriendStatus = $this->statusRepository->getUsersStatuses($param);
                    foreach ($getFriendStatus as $friendStatus) {
                        $frienrStatus[] = $friendStatus->transform($userId);
                    }
                }
            }
        }

        // $data = array_merge($personalStatus, $frienrStatus);
        // $temp = usort($data, 'compare_iso_date');
        
        $timeStampArr = [];
        foreach($personalStatus as $item){
            $timeStampArr[$item['created_at']->timestamp] = $item;
        }
        foreach($frienrStatus as $item){
            $timeStampArr[$item['created_at']->timestamp] = $item;
        }

        ksort($timeStampArr);
        foreach($timeStampArr as $item){
            $data[] = $item;
        }

        $revertData = array_reverse($data);
        return $this->successRequest($revertData);
    }
    /**
     * api xóa bài viết
     * route: [POST]api/status/delete
     * request url: http://13.67.77.166/api/status/delete
     * params:
     *      status_id: required
     * response:
     *      {
                    "error_code": 0,
                    "message": [
                        "Successfully"
                    ],
                    "data": "Thành Công"
     *}
     */
    public function deleteStatus()
    {
        $validator = \Validator::make($this->request->all(), [
            'status_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $statusId = $this->request->get('status_id');
        $checkStatus = Status::where([
            '_id' => mongo_id($statusId)
        ])->first();
        if (empty($checkStatus)) {
            return $this->errorBadRequest(trans('status.no_status'));
        }

        $checkStatus->deleted_user = Auth::getPayLoad()->get('user_id');
        $checkStatus->save();
        $checkStatus->delete();

        return $this->successRequest(trans('core.success'));
    }

    public function getStatus(){
        $validator = \Validator::make($this->request->all(),[
            'status_id' => 'required'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $statusId = $this->request->get('status_id');
        $status = Status::where([
            '_id' => mongo_id($statusId)
        ])->first();

        if(empty($status)){
            return $this->errorBadRequest(trans('status.no_status'));
        }

        return $this->successRequest($status->transform(Auth::getPayLoad()->get('user_id')));
    }
}
