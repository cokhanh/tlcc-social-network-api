<?php

namespace App\Http\Controllers\Api\V1;

use App\Api\Entities\Comments;
use App\Api\Entities\Notifications;
use App\Api\Entities\Status;
use App\Api\Entities\Users;
use App\Api\Entities\UsersInfo;
use App\Api\Repositories\Contracts\NotificationsRepository;
use App\Notifications\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Api\Repositories\Contracts\CommentsRepository;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller {
    protected $request;
    protected $notificationRepository;

    public function __construct(Request $request, NotificationsRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
        $this->request = $request;
    }

    public function add(){
        $validator = \Validator::make($this->request->all(),[
            'owner' => 'required|string',
            'type' => 'required|string',
            'item' => 'required'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $user = Auth::user();
        $userInfo = UsersInfo::where([
            '_id' => mongo_id($user->user_id)
        ])->first();
        if (empty($userInfo)) {
            return $this->errorBadRequest(trans('user.session_time_out'));
        }

        $owner = $this->request->get('owner');
        $ownerInfo = UsersInfo::where([
            '_id' => mongo_id($owner)
        ])->first();
        if(empty($ownerInfo)){
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }

        if($owner === $user->user_id){
            return $this->successRequest('You are owner');
        }

        $item = $this->request->get('item');
        $type = $this->request->get('type');

        //check if owner has had this noti
        $notiInfo = Notifications::where([
            'item' => $item,
            'owner' => $owner,
            'type' => $type
        ])->first();
        if(!empty($notiInfo)){
            $notiInfo->delete();
        }

        $content = null;
        if($type === 'status-like'){
            $content = "$user->user_name liked your status!";
        }elseif ($type === 'status-comment'){
            $content = "$user->user_name commented on your status!";
        }elseif ($type === 'friend-request'){
            $content = "$user->user_name sent you a friend request.";
        }else{
            return $this->errorBadRequest(trans('notifications.pls_check_type'));
        }

        $params = [
            'owner' => $owner,
            'type' => $type,
            'content' => $content,
            'item' => $item,
            'executed_by' => $userInfo->_id,
            'is_seen' => 0
        ];

        Notifications::create($params);
        return $this->successRequest(trans('core.success'));
    }

    public function list(){
        $validator = \Validator::make($this->request->all(),[
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $user = Auth::user();
        $userInfo = UsersInfo::where([
            '_id' => mongo_id($user->user_id)
        ])->first();
        if (empty($userInfo)) {
            return $this->errorBadRequest(trans('user.session_time_out'));
        }

        $params = [
            'owner' => $userInfo->_id
        ];
        $listNotification = $this->notificationRepository->getNotifications($params);

        $data = [];
        foreach($listNotification as $item){
            $data[] = $item->transform();
        }

        $data = array_reverse($data);
        return $this->successRequest($data);
    }
}