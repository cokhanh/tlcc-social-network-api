<?php

namespace App\Http\Controllers\Api\V1;

use App\Api\Entities\UsersInfo;
use Illuminate\Http\Request;
use App\Api\Entities\Users;
use App\Api\Entities\UserToken;
use App\Api\Repositories\Contracts\UsersRepository;
use App\Http\Controllers\Controller;
use App\Libraries\Gma\APIs\APIUpload;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\AuthManager;

class UserController extends Controller
{
    protected $request;
    protected $usersRepository;
    protected $auth;

    public function __construct(Request $request, UsersRepository $usersRepository, AuthManager $auth)
    {
        $this->request = $request;
        $this->usersRepository = $usersRepository;
        $this->auth = $auth;

        parent::__construct();
    }

    public function register()
    {
        $user_name = 'cokhanh';
        $password = '1';

        $attr = [
            'user_name' => $user_name,
            'password' => $password
        ];
        $user = Users::create($attr);
        return $this->successRequest(trans('core.success'));
    }

    public function logIn()
    {
        $validator = \Validator::make($this->request->all(), [
            'user' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        //user có thể là số điện thoại hoặc gmail
        $user = $this->request->get('user');
        $password = $this->request->get('password');

        //check user name có tồn tại
        if(is_number_string($user) == 1){
            $userAccount = Users::where([
                'phone' => $user
            ])->first();
            if (empty($userAccount)) {
                return $this->errorBadRequest(trans('user.account_not_exist'));
            }
        }else{
            $userAccount = Users::where([
                'email' => $user
            ])->first();
            if (empty($userAccount)) {
                return $this->errorBadRequest(trans('user.account_not_exist'));
            }
        }

        if (!password_verify($password, $userAccount->password)) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }

        //Nếu User có tồn tại tại thì add user vào token.
        //Lấy các thông tin của user đăng nhập
        $user_info = UsersInfo::where([
            '_id' => mongo_id($userAccount->user_id)
        ])->first();
        if (empty($user_info)) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }

        if ($user_info->phone_verified == 0) {
            $data = [];
            $data['user_name'] = $user_info->last_name . ' ' . $user_info->first_name;
            $data['phone_verified'] = false;
            $data['token'] = '';
            $data['user_avatar'] = '';
            $data['user_cover'] = '';
            $data['no_sign_profile'] = $userAccount->no_sign_profile;

            return $this->successRequest($data);
        }

        $customClaims = [];
        $customClaims['user_name'] = $userAccount->user_name;
        $customClaims['user_id'] = $userAccount->user_id;
        $customClaims['user_full_name'] = $user_info->last_name . ' ' . $user_info->first_name;
        $customClaims['phone'] = $user_info->phone;
        $customClaims['email'] = $user_info->email;
        $customClaims['sex'] = $user_info->sex;

        $token = $this->auth->customClaims($customClaims)->fromUser($userAccount);

        $attribute = [
            'user_id' => $userAccount->user_id,
            'api_token' => $token,
        ];

        UserToken::create($attribute);
        $userAccount->updated_at = Carbon::now();
        $userAccount->save();
        $data = [
            'user_name' => $user_info->last_name . ' ' . $user_info->first_name,
            'token' => $token,
            'phone_verified' => true,
            'no_sign_profile' => $userAccount->no_sign_profile
        ];
        //get user avatar
        $params = [
            'type' => 'image',
            'user_id' => $user_info->_id,
            'option' => 'avatars'
        ];
        $avatarURI = APIUpload::getFileToClient($params);
        $data['user_avatar'] = $avatarURI;

        //get user cover
        $params = [
            'type' => 'image',
            'user_id' => $user_info->_id,
            'option' => 'cover_picture'
        ];
        $coverURI = APIUpload::getFileToClient($params);
        $data['user_cover'] = $coverURI;

        return $this->successRequest($data);
    }

    public function logOut()
    {
        $validator = \Validator::make($this->request->all(), [
            'api_token' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        //client truyền token lên kèm với bearer (bearer + '' + token)
        $user = Auth::user();
        if(!empty($user)){
            $userToken = UserToken::where([
                'user_id' => $user->user_id,
                'api_token' => $this->request->get('api_token')
            ])->first();
            if (!empty($userToken)) {
                $userToken->delete();
                return $this->successRequest(trans('core.success'));
            }
        }

        return $this->errorBadRequest([]);
    }
    public function validateLogin()
    {
        $validator = \Validator::make($this->request->all(), [
            'api_token' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $user = Auth::user();
        $userToken = UserToken::where(['user_id' => $user->user_id, 'api_token' => $this->request->get('api_token')])->first();
        if (!empty($userToken)) {
            return $this->successRequest(trans('core.success'));
        }
        return $this->errorBadRequest([]);
    }

    public function getUserBasicInfo()
    {
        $userInfoList = UsersInfo::all();
        $data = [];

        foreach ($userInfoList as $item) {
            $data[] = $item->transform('get-all');
        }
        return $this->successRequest($data);
    }
}
