<?php

namespace App\Http\Controllers\Api\V1;

use App\Api\Entities\UserFriendRequest;
use App\Api\Entities\Users;
use App\Api\Entities\UsersInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Api\Repositories\Contracts\UserFriendRequestRepository;
use App\Api\Repositories\Contracts\UsersInfoRepository;
use Illuminate\Support\Facades\Auth;

class UserFriendRequestController extends Controller
{
    protected $request;
    protected $userFriendRequestRepository;

    public function __construct(
        Request $request,
        UserFriendRequestRepository $userFriendRequestRepository
    ) {
        $this->request = $request;
        $this->userFriendRequestRepository = $userFriendRequestRepository;
    }

    /**
     * api gửi yêu cầu kết bạn
     * route: [POST]api/request/make-friend
     * request url: http://13.67.77.166/api/request/make-friend
     * params:
     *      receiver: required
     * header:
     *      authorization: bearer $token
        resonse:
            {
                "error_code": 0,
                "message": [
                    "Successfully"
                ],
                "data": "Thành công"
            }
     */

    public function createFriendRequest()
    {
        $validator = \Validator::make($this->request->all(), [
            'receiver' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        //check receiver info
        $receiver = $this->request->get('receiver');
        $receiverInfo = UsersInfo::where([
            '_id' => mongo_id($receiver)
        ])->first();
        if (empty($receiverInfo)) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }

        //check sender info
        $sender = Auth::getPayLoad()->get('user_id');
        $senderInfo = UsersInfo::where([
            '_id' => mongo_id($sender)
        ])->first();
        if (empty($senderInfo)) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }

        //check not sent friend request
        $sentRequest = UserFriendRequest::where([
            'sender' => $sender,
            'receiver' => $receiver
        ])->first();
        if (!empty($sentRequest)) {
            return $this->successRequest([]);
        }

        $param = [
            'sender' => $sender,
            'receiver' => $receiver
        ];
        $friendRequest = UserFriendRequest::create($param);
        return $this->successRequest(trans('core.success'));
    }

    /**
     * api xóa yêu cầu kết bạn
     * route: [POST]api/request/delete-request
     * request url: http://13.67.77.166/api/request/delete-request
     * params:
     *      request_id: required
     * header:
     *      authorization: bearer $token
        resonse:
            {
                "error_code": 0,
                "message": [
                    "Successfully"
                ],
                "data": "Thành công"
            }
     */
    public function deleteFriendRequest()
    {
        $validator = \Validator::make($this->request->all(), [
            'request_id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $requestId = $this->request->get('request_id');
        $requestInfo = UserFriendRequest::where([
            '_id' => mongo_id($requestId)
        ])->first();
        if (empty($requestInfo)) {
            return $this->errorBadRequest(trans('friend_request.no_request'));
        }

        $requestInfo->deleted_user = Auth::getPayLoad()->get('user_id');
        $requestInfo->save();
        $requestInfo->delete();

        return $this->successRequest(trans('core.success'));
    }
    public function deleteFriendRequestFromProfilePage()
    {
        $validator = \Validator::make($this->request->all(), [
            'friend_id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->fails()->toArray());
        }

        $friendId = $this->request->get('friend_id');
        $currentUserSent = UserFriendRequest::where([
            'sender' => Auth::getPayLoad()->get('user_id'),
            'receiver' => $friendId
        ])->first();
        if (empty($currentUserSent)) {
            $currentUserReceived = UserFriendRequest::where([
                'sender' => $friendId,
                'receiver' => Auth::getPayLoad()->get('user_id')
            ])->first();
            if (empty($currentUserReceived)) {
                return $this->errorBadRequest(trans('friend_request.no_request'));
            }

            $currentUserReceived->deleted_user = Auth::getPayLoad()->get('user_id');
            $currentUserReceived->save();
            $currentUserReceived->delete();
        }
        $currentUserSent->deleted_user = Auth::getPayLoad()->get('user_id');
        $currentUserSent->save();
        $currentUserSent->delete();

        return $this->successRequest(trans('core.success'));
    }
    /**
     * api lấy danh sách yêu cầu kết bạn
     * route: [GET]api/request/list-request
     * request url: http://13.67.77.166/api/request/list-request
     * params:
     *      token: required
     * header:
     *      authorization: bearer $token
            resonse:
                {
                    "error_code": 0,
                    "message": [
                        "Successfully"
                    ],
                    "data": [
                        {
                            "id": "5fc079255c670000ab007706",
                            "receiver": "5f61c880e24500008a001072",
                            "sender": {
                                "user_name": "Khải Đỗ Tường",
                                "sender_id": "5fbdca127e80b939d21c1924",
                                "no_sign": "do-tuong-khai",
                                "sender_avatar": "http://13.67.77.166/api/get-image?name=default.jpg&option=avatars"
                            }
                        },
                        {
                            "id": "5fc0798b5c670000ab007708",
                            "receiver": "5f61c880e24500008a001072",
                            "sender": {
                                "user_name": "asd asd",
                                "sender_id": "5faa13b547ece96e2408aca2",
                                "no_sign": "asd-asd",
                                "sender_avatar": "http://13.67.77.166/api/get-image?name=default.jpg&option=avatars"
                            }
                        }
                    ]
                }
     */
    public function listFriendRequest()
    {
        $validator = \Validator::make($this->request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $userInfo = UsersInfo::where([
            '_id' => mongo_id(Auth::getPayLoad()->get('user_id'))
        ]);
        if (empty($userInfo)) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }

        $listRequest = $this->userFriendRequestRepository->getListFriendRequest();
        $data = [];
        foreach ($listRequest as $item) {
            $data[] = $item->transform();
        }

        return $this->successRequest($data);
    }
    /**
     * api huỷ kết bạn
     * route: [POST]api/request/unfriend
     * request url: http://13.67.77.166/api/request/unfriend
     * params:
     *      friend_id: required
     * header:
     *      authorization: bearer $token
            resonse:
                {
                    "error_code": 0,
                    "message": [
                        "Successfully"
                    ],
                    "data": "Thành Công"
                }
     */
    public function unfriend()
    {
        $validator = \Validator::make($this->request->all(), [
            'friend_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $friendId = $this->request->get('friend_id');
        $friendInfo = UsersInfo::where([
            '_id' => mongo_id($friendId)
        ])->first();
        if (empty($friendInfo)) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }

        $friendOfFriendList = (array)($friendInfo->list_friends);
        for ($i = 0; $i < count($friendOfFriendList); $i++) {
            if ($friendOfFriendList[$i] == Auth::getPayLoad()->get('user_id')) {
                $friendOfFriendList[$i] = 'Undefined';
                break;
            }
        }

        $userInfo = UsersInfo::where([
            '_id' => mongo_id(Auth::getPayLoad()->get('user_id'))
        ])->first();
        $userFriendList = (array)$userInfo->list_friends;
        for ($i = 0; $i < count($userFriendList); $i++) {
            if ($userFriendList[$i] == $friendId) {
                $userFriendList[$i] = 'Undefined';
                break;
            }
        }

        $friendInfo->list_friends = $friendOfFriendList;
        $userInfo->list_friends = $userFriendList;
        $friendInfo->save();
        $userInfo->save();

        return $this->successRequest(trans('core.success'));
    }
}
