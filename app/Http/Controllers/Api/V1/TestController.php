<?php

namespace App\Http\Controllers\Api\V1;

use App\Api\Entities\Branch;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Api\Entities\Test;
use App\Api\Entities\Upload;
use App\Libraries\Gma\APIs\APIAuth;
use App\Libraries\Gma\APIs\APIUpload;
use App\Libraries\Gma\APIs\APIWifi;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Twilio\Rest\Client;
use Intervention\Image\ImageManagerStatic as Image;

class TestController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function testDBConnect()
    {
        $attr = [
            'branch_name' => 'Khánh',
            'phone' => '21'
        ];
        $branch = Branch::create($attr);

        return $this->successRequest($branch);
    }
    //prevent app from sleeping
    public function test()
    {
        return $this->successRequest('From KKH Team with love');
    }
    //get client ip
    public function testWifi()
    {
        djson($this->request->ip());
    }

    public function store()
    {
        //lấy record gần nhất
        // $temp = Upload::where([
        //     'type' => 'image',
        //     'user_id' => '5f5c5c17ba3700002b00501c',
        //     'option' => 'avatars'
        // ])->orderBy('created_at', 'desc')->first();
        // return $this->successRequest($temp);
        $validator = \Validator::make($this->request->all(), [
            'file' => 'file'
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $param = [
            'user_id' => $this->request->get('user_id'),
            'type' => $this->request->get('type'),
            'option' => $this->request->get('option')
        ];
        $file = $this->request->file('file');


        APIUpload::uploadToServer($param, $file);
        // $data = $this->request->input('image');
        // $photo = $this->request->file('image')->getClientOriginalName();
        // $destination = base_path() . '/public/img';
        // $temp = $this->request->file('image')->move($destination, $photo);

        return $this->successRequest('OK');
    }
    //gọi API trả về đường link lấy ảnh
    public function getImage()
    {
        //dd('sd');
        $params = [
            'type' => 'image',
            'user_id' => '5f61c880e24500008a001072',
            'option' => 'avatars'
        ];
        //trả về full path
        $temp = APIUpload::getFileToClient($params);
        return $this->successRequest($temp);
    }

    public function loadImage()
    {
        $validator = \Validator::make($this->request->all(), [
            'name' => 'required',
            'option' => [
                'required',
                Rule::in(['avatars', 'cover_picture', 'status', 'chat'])
            ]
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $img_name = $this->request->get('name');
        $option = $this->request->get('option');

        $filename = basename($img_name);
        $file_extension = strtolower(substr(strrchr($filename, "."), 1));

        if (validate_image_extention('image/' . $file_extension)) {
            $headers = ['Content-Type' => 'image/' . $file_extension];
            $path = '../public/image/' . $option . '/' . $filename;
            //kiểm tra có path trong source
            if (file_exists($path)) {
                $response = new BinaryFileResponse($path, 200, $headers);
                return $response;
            }
            
            $path = '../public/image/' . $option . '/default.jpg';
            $response = new BinaryFileResponse($path, 200, $headers);
            return $response;
        }else{
            $headers = ['Content-Type' => 'video/' . $file_extension . '; video/mpeg'];
            $path = '../public/video/' . $option . '/' . $filename;
            //kiểm tra có path trong source
            if (file_exists($path)) {
                $response = new BinaryFileResponse($path, 200, $headers);
                return $response;
            }
            
            $path = '../public/video/' . $option . '/default.jpg';
            $response = new BinaryFileResponse($path, 200, $headers);
            return $response;
        }
        return $this->errorBadRequest(trans('core.invalid_format'));
    }
    public function sms()
    {
        // $account_sid = 'AC2733f9b5975f3d5d3273bdbbd2392f40';
        // $auth_token = '39c6c3a15c87f2592dc914342d696250';

        // $twilio_number = '+12016854932';

        // //các số điện thoại sẽ nhận tin nhắn
        // //do là bản xài thử nên chỉ có thể tự gửi sms cho chính mình
        // $client = new Client($account_sid,$auth_token);
        // $client->messages->create(
        //     '+84563243215',
        //     array(
        //         'from'=>$twilio_number,
        //         'body'=>'Helloooo'
        //     )
        //     );
        APIAuth::smsTwilio();
        return $this->successRequest(trans('core.success'));
    }

    public function testUbuntuUsage(){
        
    }

    public function image(){
        $validator = \Validator::make($this->request->all(),[
            'file' => 'required'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $file = $this->request->get("file");

        //kiểm tra có path trong source
        if (file_exists(public_path() ."/image/$file")) {
            return \Illuminate\Support\Facades\File::get(public_path() . "/image/$file");
        }

        return \Illuminate\Support\Facades\File::get(public_path() . "/image/default.jpg");
    }
    public function video(){
        $validator = \Validator::make($this->request->all(),[
            'file' => 'required'
        ]);

        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $fileName = $this->request->get("file");
        $file_extension = strtolower(substr(strrchr($fileName, "."), 1));

        $headers = ['Content-Type' => 'video/' . $file_extension . '; video/mpeg'];
//        $path =
        //kiểm tra có path trong source
        if (file_exists(public_path() ."/video/$fileName")) {
            $response = new BinaryFileResponse(public_path() ."/video/$fileName", 200, $headers);
            return $response;
        }

        $path = public_path() ."/video/default.mp4";
        $response = new BinaryFileResponse($path, 200, $headers);
        return $response;
    }
}
