<?php

namespace App\Http\Middleware;

use App\Api\Entities\Chat;
use Closure;
use Illuminate\Support\Facades\Auth;

class ChatMiddleware
{
    public function handle($request, Closure $next)
    {
        $userId = $request->input('user_id');
        $currentUser = Auth::getPayLoad()->get('user_id');

        $chatRoom = Chat::where('user_ids', $userId)->where('user_ids', $currentUser)->first();

        if (empty($chatRoom)) {
            return $next($request);
        }

        return response([
            'error_code' => 0,
            'message' => ['Successfully'],
            'data' => $chatRoom->_id,
        ], 200);
    }
}
