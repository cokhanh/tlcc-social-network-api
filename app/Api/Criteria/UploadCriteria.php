<?php

namespace App\Api\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\Auth;
/**
 * Class UploadCriteria
 */
class UploadCriteria implements CriteriaInterface
{
    protected $params;
    public function __construct($params = [])
    {
        $this->params = $params;
    }
    
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $query = $model->newQuery();

        if(!empty($this->params['_id'])){
            $query->where('_id',mongo_id($this->params['_id']));
        }
        if(!empty($this->params['org_id'])){
            $query->where('org_id',$this->params['org_id']);
        }
        if(!empty($this->params['branch_id'])){
            $query->where('branch_id',$this->params['branch_id']);
        }
        if(!empty($this->params['user_id'])){
            $query->where('user_id',$this->params['user_id']);
        }
        if(!empty($this->params['type'])){
            $query->where('type',$this->params['type']);
        }
        if(!empty($this->params['extension'])){
            $query->where('extension',$this->params['extension']);
        }
        if(!empty($this->params['path'])){
            $path = '%'.$this->params['path'].'%';
            $query->where('path','like',$path);
        }
        return $query;
    }
}
