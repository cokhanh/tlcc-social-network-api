<?php

namespace App\Api\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\Auth;

/**
 * Class StatusCriteria
 */
class StatusCriteria implements CriteriaInterface
{
    protected $params;
    public function __construct($params = [])
    {
        $this->params = $params;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $query = $model->newQuery();

        if (!empty($this->params['user_id'])) {
            $query->where('user_id', $this->params['user_id']);
        }
        if (!empty($this->params['status_setting'])) {
            $query->where('status_setting', $this->params['status_setting']);
        }

        return $query;
    }
}
