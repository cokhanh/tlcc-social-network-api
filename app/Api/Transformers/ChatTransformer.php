<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Chat;
use App\Api\Entities\ChatMessages;
use App\Api\Entities\Users;
use App\Api\Entities\UsersInfo;
use App\Libraries\Gma\APIs\APIUpload;

/**
 * Class ChatTransformer
 */
class ChatTransformer extends TransformerAbstract
{

    /**
     * Transform the \Chat entity
     * @param \Chat $model
     *
     * @return array
     */
    public function transform(Chat $model, string $type = '', string $userId = '')
    {
        $data = [
            'id' => $model->_id,
            'room' => $model->room_name,
            'messRecent' => '',
            'time' => '',

        ];

        $currentUser = UsersInfo::where([
            '_id' => mongo_id($userId)
        ])->first();
        if (!empty($currentUser)) {
            $data['userName'] = $currentUser->last_name . ' ' . $currentUser->first_name;
        }

        $userIds = $model->user_ids;
        for ($i = 0; $i < count($userIds); $i++) {
            if ($userId != $userIds[$i]) {
                $partnerInfo = UsersInfo::where([
                    '_id' => mongo_id($userIds[$i])
                ])->first();
                if (!empty($partnerInfo)) {
                    $data['friend_chat'] = $partnerInfo->last_name . ' ' . $partnerInfo->first_name;
                    $user = Users::where([
                        'user_id' => $partnerInfo->_id
                    ])->first();
                    $data['partner_no_sign_profile'] = $user->no_sign_profile;

                    $params = [
                        'type' => 'image',
                        'user_id' => $partnerInfo->_id,
                        'option' => 'avatars'
                    ];
                    $avatarURI = APIUpload::getFileToClient($params);
                    $data['avatar'] = $avatarURI;
                }
            }
        }

        $chatMessages = ChatMessages::where([
            'chat_group_id' => $data['id']
        ])->orderBy('created_at', 'desc')->first();
        if (!empty($chatMessages)) {
            $data['messRecent'] = $chatMessages->contents;
            $data['time'] = date("d/m/Y h:i:s A", strtotime(date($chatMessages->created_at)));

            if ($chatMessages->owner == $userId) {
                $data['isCurrent'] = true;
            } else {
                $data['isCurrent'] = false;
            }
        }
        return $data;
    }
}
