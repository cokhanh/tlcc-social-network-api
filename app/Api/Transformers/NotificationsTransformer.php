<?php

namespace App\Api\Transformers;

use App\Libraries\Gma\APIs\APIUpload;
use League\Fractal\TransformerAbstract;
use App\Api\Entities\Notifications;

/**
 * Class NotificationsTransformer
 */
class NotificationsTransformer extends TransformerAbstract
{

    /**
     * Transform the \Notifications entity
     * @param \Notifications $model
     *
     * @return array
     */
    public function transform(Notifications $model)
    {
        $data = [
            'id' => $model->_id,
            'content' => $model->content,
            'is_seen' => $model->is_seen,
            'type' => $model->type,
            'item' => $model->item,
            'owner' => $model->owner,
            'created_at' => date("Ymdhis", strtotime(date($model->created_at)))
        ];

        //get user avatar
        $params = [
            'type' => 'image',
            'user_id' => $model->executed_by,
            'option' => 'avatars'
        ];
        $avatarURI = APIUpload::getFileToClient($params);
        $data['friend_avatar'] = $avatarURI;

        return $data;
    }
}
