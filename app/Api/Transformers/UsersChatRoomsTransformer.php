<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\UsersChatRooms;

/**
 * Class UsersChatRoomsTransformer
 */
class UsersChatRoomsTransformer extends TransformerAbstract
{

    /**
     * Transform the \UsersChatRooms entity
     * @param \UsersChatRooms $model
     *
     * @return array
     */
    public function transform(UsersChatRooms $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
