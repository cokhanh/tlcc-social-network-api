<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Newsfeed;

/**
 * Class NewsfeedTransformer
 */
class NewsfeedTransformer extends TransformerAbstract
{

    /**
     * Transform the \Newsfeed entity
     * @param \Newsfeed $model
     *
     * @return array
     */
    public function transform(Newsfeed $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
