<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\UserFriendRequest;
use App\Api\Entities\Users;
use App\Api\Entities\UsersInfo;
use App\Libraries\Gma\APIs\APIUpload;

/**
 * Class UserFriendRequestTransformer
 */
class UserFriendRequestTransformer extends TransformerAbstract
{

    /**
     * Transform the \UserFriendRequest entity
     * @param \UserFriendRequest $model
     *
     * @return array
     */
    public function transform(UserFriendRequest $model)
    {
        $data = [
            'id' => $model->_id,
            'receiver' => $model->receiver
        ];

        $senderId = $model->sender;
        $senderInfo = UsersInfo::where([
            '_id' => mongo_id($senderId)
        ])->first();
        if (!empty($senderInfo)) {
            $senderDetail = [];
            $senderAccount = Users::where([
                'user_id' => $senderId
            ])->first();
            if (!empty($senderAccount)) {
                $senderDetail['user_name'] = $senderInfo->last_name . ' ' . $senderInfo->first_name;
                $senderDetail['sender_id'] = $senderInfo->_id;
                $senderDetail['no_sign'] = $senderAccount->no_sign;
                $senderDetail['no_sign_profile'] = $senderAccount->no_sign_profile;

                //get user avatar
                $params = [
                    'type' => 'image',
                    'user_id' => $senderId,
                    'option' => 'avatars'
                ];
                $avatarURI = APIUpload::getFileToClient($params);
                $senderDetail['sender_avatar'] = $avatarURI;
                $data['sender'] = $senderDetail;
            }
        }
        return $data;
    }
}
