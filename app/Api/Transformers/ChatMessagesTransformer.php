<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\ChatMessages;
use App\Api\Entities\Users;
use App\Api\Entities\UsersInfo;
use App\Libraries\Gma\APIs\APIUpload;

/**
 * Class ChatMessagesTransformer
 */
class ChatMessagesTransformer extends TransformerAbstract
{

    /**
     * Transform the \ChatMessages entity
     * @param \ChatMessages $model
     *
     * @return array
     */
    public function transform(ChatMessages $model, string $type = '')
    {
        $data = [
            'id' => $model->_id,
            'contents' => $model->contents
        ];

        $ownerInfo = Users::where([
            'user_id' => $model->owner
        ])->first();
        if (!empty($ownerInfo)) {
            $data['owner'] = $ownerInfo->no_sign_profile;
            $data['user_name'] = $ownerInfo->getUserName();

            //get user avatar
            $params = [
                'type' => 'image',
                'user_id' => $model->owner,
                'option' => 'avatars'
            ];
            $avatarURI = APIUpload::getFileToClient($params);
            $data['user_avatar'] = $avatarURI;
        }

        return $data;
    }
}
