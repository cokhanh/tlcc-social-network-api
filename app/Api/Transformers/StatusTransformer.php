<?php

namespace App\Api\Transformers;

use App\Api\Entities\Comments;
use League\Fractal\TransformerAbstract;
use App\Api\Entities\Status;
use App\Api\Entities\Upload;
use App\Api\Entities\Users;
use App\Api\Entities\UsersInfo;
use App\Libraries\Gma\APIs\APIUpload;
use Carbon\Carbon;
use DateTime;

/**
 * Class StatusTransformer
 */
class StatusTransformer extends TransformerAbstract
{

    /**
     * Transform the \Status entity
     * @param \Status $model
     *
     * @return array
     */
    public function transform(Status $model, string $userInView)
    {
        $data = [
            'id' => $model->_id,
            'user_id' => $model->user_id,
            'status_setting' => $model->status_setting,
            'caption' => $model->caption,
            'created_at' => $model->created_at
        ];
        if (!empty($model->created_at)) {
            $data['posted_time'] = date("d/m/Y h:i:s A", strtotime(date($model->created_at)));
        }
        $userInfo = Users::where([
            'user_id' => $model->user_id
        ])->first();
        if (!empty($userInfo)) {
            $data['user_name'] = $userInfo->user_name;
            $data['no_sign_profile'] = $userInfo->no_sign_profile;

            $user = UsersInfo::where([
                '_id' => mongo_id($model->user_id)
            ])->first();
            if(!empty($user)){
                $data['sex'] = $user->sex;
            }else{
                return trans('core.norecord');
            }
        }

        if (isset($model->like)) {
            $data['like_number'] = $model->like;
        } else {
            $data['like_number'] = 0;
        }

        //get user avatar
        $params = [
            'type' => 'image',
            'user_id' => $data['user_id'],
            'option' => 'avatars'
        ];
        $avatarURI = APIUpload::getFileToClient($params);
        $data['user_avatar'] = $avatarURI;

        //check have comment
        //get link

        if (isset($model->upload_ids)) {
            $statusImageArr = [];
            $uploadIdsArr = $model->upload_ids;

            foreach ($uploadIdsArr as $item) {
                $uploadInfo = Upload::where([
                    '_id' => mongo_id($item)
                ])->first();


                if (!empty($uploadInfo)) {
                    $params = [
                        'type' => 'image',
                        'user_id' => $data['user_id'],
                        'option' => $uploadInfo->option,
                        'upload_id' => $item,
                        'is_status' => 1
                    ];

                    $postedURI = APIUpload::getFileToClient($params);
                    array_push($statusImageArr, $postedURI);
                }
            }
            $data['file_uploaded'] = $statusImageArr;
        } else {
            $data['file_uploaded'] = [];
        }
        //liked status
        if (isset($model->like)) {
            $userLikedDetailInfo = [];
            $userLikeArr = $model->user_like;
            
            foreach ($userLikeArr as $item) {
                if ($userInView == $item) {
                    $data['liked'] = true;
                    break;
                }
            }
            //danh sách user đã like status
            foreach ($userLikeArr as $item) {
                if ($item != 'Undefined') {
                    $params = [
                        'type' => 'image',
                        'user_id' => $item,
                        'option' => 'avatars'
                    ];
                    $avatarFriendURI = APIUpload::getFileToClient($params);

                    $detailInfo = UsersInfo::where([
                        '_id' => mongo_id($item)
                    ])->first();
                    if (!empty($detailInfo)) {
                        $userName = $detailInfo->last_name . ' ' . $detailInfo->first_name;
                        $userId = $detailInfo->_id;
                    }

                    $userDetail = [
                        'avatar' => $avatarFriendURI,
                        'user_name' => $userName,
                        'user_id' => $userId
                    ];
                    array_push($userLikedDetailInfo, $userDetail);
                }
            }
            $data['who_liked_status'] = $userLikedDetailInfo;
        } else {
            $data['who_liked_status'] = [];
        }

        if (!isset($data['liked'])) {
            $data['liked'] = false;
        }

        $headerContent = $model->header_content;
        if (isset($headerContent)) {
            if ($headerContent == 0) {
                if ($data['sex'] == 0) {
                    $data['header_content'] = trans('status.had_updated_avatar') . ' ' . trans('user.she');
                } else {
                    $data['header_content'] = trans('status.had_updated_avatar') . ' ' . trans('user.he');
                }
            } elseif ($headerContent == 1) {
                if ($data['sex'] == 0) {
                    $data['header_content'] = trans('status.had_updated_cover') . ' ' . trans('user.she');
                } else {
                    $data['header_content'] = trans('status.had_updated_cover') . ' ' . trans('user.he');
                }
            }
        }

        //get comment's status
        $comments = [];
        $statusId = $model->_id;
        $statusInfo = Status::where([
            '_id' => mongo_id($statusId)
        ])->first();

        if (!empty($statusInfo)) {
            $commentsList = Comments::where([
                'status_id' => $statusId
            ])->get();

            foreach($commentsList as $comment){
                $comments[] = $comment->transform();
            }
        }

        $data['comments'] = $comments;

        return $data;
    }
}
