<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\PageGroup;

/**
 * Class PageGroupTransformer
 */
class PageGroupTransformer extends TransformerAbstract
{

    /**
     * Transform the \PageGroup entity
     * @param \PageGroup $model
     *
     * @return array
     */
    public function transform(PageGroup $model)
    {
        return [
            'id'         => $model->_id,

            

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
