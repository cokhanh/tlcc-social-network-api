<?php

namespace App\Api\Transformers;

use App\Api\Entities\Users;
use App\Api\Entities\UsersInfo;
use App\Libraries\Gma\APIs\APIUpload;
use League\Fractal\TransformerAbstract;
use App\Api\Entities\Comments;

/**
 * Class CommentsTransformer
 */
class CommentsTransformer extends TransformerAbstract
{

    /**
     * Transform the \Comments entity
     * @param \Comments $model
     *
     * @return array
     */
    public function transform(Comments $model)
    {
        $data = [
            'id' => $model->_id,
            'status_id' => $model->status_id,
            'user' => $model->user,
            'comment' => $model->content,
        ];

        $user = UsersInfo::where([
            '_id' => mongo_id($model->user)
        ])->first();
        $userNosignProfile = Users::where([
            'user_id' => $model->user
        ])->first();
        if(empty($user) || empty($userNosignProfile)){
           return;
        }

        //get user avatar
        $params = [
            'type' => 'image',
            'user_id' => $model->user,
            'option' => 'avatars'
        ];
        $avatarURI = APIUpload::getFileToClient($params);
        $data['user_avatar'] = $avatarURI;
        $data['linkProfile'] = $userNosignProfile->no_sign_profile;
        $data['user_name'] = $user->last_name . ' ' . $user->first_name;

        return $data;
    }
}
