<?php

namespace App\Api\Transformers;

use App\Api\Entities\Users;
use League\Fractal\TransformerAbstract;
use App\Api\Entities\UsersInfo;
use App\Libraries\Gma\APIs\APIUpload;
use DateTime;

/**
 * Class UsersInfoTransformer
 */
class UsersInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the \UsersInfo entity
     * @param \UsersInfo $model
     *
     * @return array
     */
    public function transform(UsersInfo $model, string $type = '')
    {
        $data = [
            'user_id' => $model->_id,
            'first_name' => $model->first_name,
            'last_name' => $model->last_name,
            'email' => $model->email,
            'phone' => $model->phone
        ];

        $createDateTime = DateTime::createFromFormat("d/m/Y", $model->dOb);
        $data['dOb'] = $createDateTime->format('d/m/Y');

        $userAccount = Users::where([
            'user_id' => $data['user_id']
        ])->first();
        if (!empty($userAccount)) {
            $data['user_name'] = $model->last_name . ' ' . $model->first_name;
            $data['no_sign'] = $userAccount->no_sign;
            $data['no_sign_profile'] = $userAccount->no_sign_profile;
        }

        if ($model->sex == 0) {
            $data['sex'] = false;
        } else {
            $data['sex'] = true;
        }

        //get user avatar
        $params = [
            'type' => 'image',
            'user_id' => $data['user_id'],
            'option' => 'avatars'
        ];
        $avatarURI = APIUpload::getFileToClient($params);
        $data['user_avatar'] = $avatarURI;

        //get user cover
        $params = [
            'type' => 'image',
            'user_id' => $data['user_id'],
            'option' => 'cover_picture'
        ];
        $coverURI = APIUpload::getFileToClient($params);
        $data['user_cover'] = $coverURI;

        //get list friend and count friend
        if (isset($model->list_friends)) {
            $friendDetailArr = [];
            $listFriends = $model->list_friends;
            foreach ($listFriends as $item) {
                if ($item != 'Undefined') {
                    $params = [
                        'type' => 'image',
                        'user_id' => $item,
                        'option' => 'avatars'
                    ];
                    $avatarFriendURI = APIUpload::getFileToClient($params);

                    $params = [
                        'type' => 'image',
                        'user_id' => $item,
                        'option' => 'cover_picture'
                    ];
                    $coverFriendURI = APIUpload::getFileToClient($params);

                    $friendInfo = UsersInfo::where([
                        '_id' => mongo_id($item)
                    ])->first();
                    if (!empty($friendInfo)) {
                        $userName = $friendInfo->last_name . ' ' . $friendInfo->first_name;
                        $userId = $friendInfo->_id;
                    }

                    $friendAccount = Users::where([
                        'user_id' => $item
                    ])->first();
                    if (!empty($friendAccount)) {
                        $noSign = $friendAccount->no_sign;
                        $noSignProfile = $friendAccount->no_sign_profile;
                    }

                    $friend = [
                        'avatar' => $avatarFriendURI,
                        'cover' => $coverFriendURI,
                        'user_name' => $userName,
                        'no_sign' => $noSign,
                        'no_sign_profile' => $noSignProfile,
                        'user_id' => $userId
                    ];
                    array_push($friendDetailArr, $friend);
                }
            }
            $data['friend_array'] = $friendDetailArr;
            $data['number_of_friend'] = count($listFriends);
        }

        if ($type == 'get-all') {
            $data = [
                'user_id' => $model->_id,
                'no_sign' => $userAccount->no_sign,
                'no_sign_profile' => $userAccount->no_sign_profile
            ];
        }

        return $data;
    }
}
