<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ChatMessagesRepository
 */
interface ChatMessagesRepository extends RepositoryInterface
{
    public function getChatMessages($params = [], $limit = 0);
}
