<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ChatRepository
 */
interface ChatRepository extends RepositoryInterface
{
    public function getChatGroup($params = [], $limit = 0);
    public function getChatMessages($params = []);
}
