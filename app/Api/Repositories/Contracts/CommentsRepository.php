<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CommentsRepository
 */
interface CommentsRepository extends RepositoryInterface
{
    public function getComments($params = [], $limit = 0);
}
