<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UsersInfoRepository
 */
interface UsersInfoRepository extends RepositoryInterface
{
    public function getUserInfo($params = [], $limit = 0);
}
