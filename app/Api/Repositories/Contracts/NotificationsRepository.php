<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NotificationsRepository
 */
interface NotificationsRepository extends RepositoryInterface
{
    public function getNotifications($params = [],$limit = 0);
}
