<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UsersChatRoomsRepository
 */
interface UsersChatRoomsRepository extends RepositoryInterface
{
    
}
