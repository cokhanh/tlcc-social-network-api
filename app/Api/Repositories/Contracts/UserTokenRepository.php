<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserTokenRepository
 */
interface UserTokenRepository extends RepositoryInterface
{
    
}
