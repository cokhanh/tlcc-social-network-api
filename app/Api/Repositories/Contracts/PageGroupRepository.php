<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PageGroupRepository
 */
interface PageGroupRepository extends RepositoryInterface
{
    
}
