<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface StatusRepository
 */
interface StatusRepository extends RepositoryInterface
{
    public function getUsersStatuses($params = [], $limit = 0);
}
