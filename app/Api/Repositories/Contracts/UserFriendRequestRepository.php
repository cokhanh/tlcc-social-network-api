<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserFriendRequestRepository
 */
interface UserFriendRequestRepository extends RepositoryInterface
{
    public function getListFriendRequest($params = [], $limit = 0);
}
