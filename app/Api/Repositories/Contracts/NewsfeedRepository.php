<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NewsfeedRepository
 */
interface NewsfeedRepository extends RepositoryInterface
{
    
}
