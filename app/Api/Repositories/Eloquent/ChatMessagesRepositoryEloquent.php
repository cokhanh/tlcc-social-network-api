<?php

namespace App\Api\Repositories\Eloquent;

use App\Api\Criteria\ChatMessagesCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\chatMessagesRepository;
use App\Api\Entities\ChatMessages;
use App\Api\Validators\ChatMessagesValidator;

/**
 * Class ChatMessagesRepositoryEloquent
 */
class ChatMessagesRepositoryEloquent extends BaseRepository implements ChatMessagesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ChatMessages::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
    public function getChatMessages($params = [], $limit = 0)
    {
        $this->pushCriteria(new ChatMessagesCriteria($params));

        if (!empty($params['is_detail'])) {
            $item = $this->get()->first();
        } elseif (!empty($params['is_paginate'])) {
            if ($limit != 0) {
                $item = $this->paginate($limit);
            } else {
                $item = $this->paginate();
            }
        } else {
            $item = $this->all();
        }
        $this->popCriteria(new ChatMessagesCriteria($params));
        return $item;
    }
}
