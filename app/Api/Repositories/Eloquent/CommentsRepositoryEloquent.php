<?php

namespace App\Api\Repositories\Eloquent;

use App\Api\Criteria\CommentsCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\commentsRepository;
use App\Api\Entities\Comments;
use App\Api\Validators\CommentsValidator;

/**
 * Class CommentsRepositoryEloquent
 */
class CommentsRepositoryEloquent extends BaseRepository implements CommentsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Comments::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
    public function getComments($params = [], $limit = 0)
    {
        $this->pushCriteria(new CommentsCriteria($params));

        if (!empty($params['is_detail'])) {
            $item = $this->get()->first();
        } elseif (!empty($params['is_paginate'])) {
            if ($limit != 0) {
                $item = $this->paginate($limit);
            } else {
                $item = $this->paginate();
            }
        } else {
            $item = $this->all();
        }
        $this->popCriteria(new CommentsCriteria($params));
        return $item;
    }
}
