<?php

namespace App\Api\Repositories\Eloquent;

use App\Api\Criteria\NotificationsCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\notificationsRepository;
use App\Api\Entities\Notifications;
use App\Api\Validators\NotificationsValidator;

/**
 * Class NotificationsRepositoryEloquent
 */
class NotificationsRepositoryEloquent extends BaseRepository implements NotificationsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Notifications::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }

    public function getNotifications($params = [],$limit = 0){
        $this->pushCriteria(new NotificationsCriteria($params));

        if(!empty($params['is_detail'])){
            $item = $this->get()->first();
        }elseif(!empty($params['is_paginate'])){
            if($limit != 0){
                $item = $this->paginate($limit);
            }else{
                $item = $this->paginate();
            }
        }else{
            $item = $this->all();
        }

        $this->popCriteria(new NotificationsCriteria($params));
        return $item;
    }
}
