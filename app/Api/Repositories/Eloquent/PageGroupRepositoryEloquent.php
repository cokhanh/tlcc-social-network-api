<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\pageGroupRepository;
use App\Api\Entities\PageGroup;
use App\Api\Validators\PageGroupValidator;

/**
 * Class PageGroupRepositoryEloquent
 */
class PageGroupRepositoryEloquent extends BaseRepository implements PageGroupRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PageGroup::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
