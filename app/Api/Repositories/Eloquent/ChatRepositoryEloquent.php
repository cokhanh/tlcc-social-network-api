<?php

namespace App\Api\Repositories\Eloquent;

use App\Api\Criteria\ChatCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\chatRepository;
use App\Api\Entities\Chat;
use App\Api\Validators\ChatValidator;

/**
 * Class ChatRepositoryEloquent
 */
class ChatRepositoryEloquent extends BaseRepository implements ChatRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Chat::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
    public function getChatGroup($params = [], $limit = 0)
    {
        $this->pushCriteria(new ChatCriteria($params));

        if (!empty($params['is_detail'])) {
            $item = $this->get()->first();
        } elseif (!empty($params['is_paginate'])) {
            if ($limit != 0) {
                $item = $this->paginate($limit);
            } else {
                $item = $this->paginate();
            }
        } else {
            $item = $this->all();
        }
        $this->popCriteria(new ChatCriteria($params));
        return $item;
    }
    public function getChatMessages($params = [])
    {
    }
}
