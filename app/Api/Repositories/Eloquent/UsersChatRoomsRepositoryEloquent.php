<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\usersChatRoomsRepository;
use App\Api\Entities\UsersChatRooms;
use App\Api\Validators\UsersChatRoomsValidator;

/**
 * Class UsersChatRoomsRepositoryEloquent
 */
class UsersChatRoomsRepositoryEloquent extends BaseRepository implements UsersChatRoomsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UsersChatRooms::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
