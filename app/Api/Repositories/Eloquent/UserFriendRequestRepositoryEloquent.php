<?php

namespace App\Api\Repositories\Eloquent;

use App\Api\Criteria\UserFriendRequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\userFriendRequestRepository;
use App\Api\Entities\UserFriendRequest;
use App\Api\Validators\UserFriendRequestValidator;

/**
 * Class UserFriendRequestRepositoryEloquent
 */
class UserFriendRequestRepositoryEloquent extends BaseRepository implements UserFriendRequestRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserFriendRequest::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
    public function getListFriendRequest($params = [], $limit = 0)
    {
        $this->pushCriteria(new UserFriendRequestCriteria($params));

        if (!empty($params['is_detail'])) {
            $item = $this->get()->first();
        } elseif (!empty($params['is_paginate'])) {
            if ($limit != 0) {
                $item = $this->paginate($limit);
            } else {
                $item = $this->paginate();
            }
        } else {
            $item = $this->all();
        }
        $this->popCriteria(new UserFriendRequestCriteria($params));
        return $item;
    }
}
