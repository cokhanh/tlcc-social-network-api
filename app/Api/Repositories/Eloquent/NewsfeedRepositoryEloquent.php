<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\newsfeedRepository;
use App\Api\Entities\Newsfeed;
use App\Api\Validators\NewsfeedValidator;

/**
 * Class NewsfeedRepositoryEloquent
 */
class NewsfeedRepositoryEloquent extends BaseRepository implements NewsfeedRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Newsfeed::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
