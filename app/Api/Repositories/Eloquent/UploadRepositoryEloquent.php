<?php

namespace App\Api\Repositories\Eloquent;

use App\Api\Criteria\UploadCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\uploadRepository;
use App\Api\Entities\Upload;
use App\Api\Validators\UploadValidator;

/**
 * Class UploadRepositoryEloquent
 */
class UploadRepositoryEloquent extends BaseRepository implements UploadRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Upload::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }

    public function getUploadedFiles($params = [],$limit = 0){
        $this->pushCriteria(new UploadCriteria($params));

        if(!empty($params['is_detail'])){
            $item = $this->get()->first();
        }elseif(!empty($params['is_paginate'])){
            if($limit != 0){
                $item = $this->paginate($limit);
            }else{
                $item = $this->paginate();
            }
        }else{
            $item = $this->all();
        }

        $this->popCriteria(new UploadCriteria($params));
        return $item;
    }
}
