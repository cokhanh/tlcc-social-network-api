<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\PageGroupTransformer;
use Moloquent\Eloquent\SoftDeletes;

class PageGroup extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'page_group';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new PageGroupTransformer();

        return $transformer->transform($this);
    }

}
