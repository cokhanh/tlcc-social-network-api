<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\ChatTransformer;
use Moloquent\Eloquent\SoftDeletes;

class Chat extends Moloquent
{
    use SoftDeletes;

    protected $collection = 'chat_groups';

    protected $guarded = [];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform(string $type = '', string $userId = '')
    {
        $transformer = new ChatTransformer();

        return $transformer->transform($this, $type, $userId);
    }
}
