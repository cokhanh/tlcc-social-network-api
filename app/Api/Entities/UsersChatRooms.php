<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\UsersChatRoomsTransformer;
use Moloquent\Eloquent\SoftDeletes;

class UsersChatRooms extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'users_chat_rooms';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new UsersChatRoomsTransformer();

        return $transformer->transform($this);
    }

}
