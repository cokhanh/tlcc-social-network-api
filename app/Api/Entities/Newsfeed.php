<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\NewsfeedTransformer;
use Moloquent\Eloquent\SoftDeletes;

class Newsfeed extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'news_feed';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new NewsfeedTransformer();

        return $transformer->transform($this);
    }

}
