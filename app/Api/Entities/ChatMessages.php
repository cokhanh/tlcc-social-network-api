<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\ChatMessagesTransformer;
use Moloquent\Eloquent\SoftDeletes;

class ChatMessages extends Moloquent
{
    use SoftDeletes;

    protected $collection = 'chat_messages';

    protected $guarded = [];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform(string $type = '')
    {
        $transformer = new ChatMessagesTransformer();

        return $transformer->transform($this, $type);
    }
}
