<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\NotificationsTransformer;
use Moloquent\Eloquent\SoftDeletes;

class Notifications extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'notifications';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new NotificationsTransformer();

        return $transformer->transform($this);
    }

}
