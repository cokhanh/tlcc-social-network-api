<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\StatusTransformer;
use Moloquent\Eloquent\SoftDeletes;

class Status extends Moloquent
{
    use SoftDeletes;

    protected $collection = 'status';

    protected $guarded = array();

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform(string $userInView)
    {
        $transformer = new StatusTransformer();

        return $transformer->transform($this, $userInView);
    }
}
