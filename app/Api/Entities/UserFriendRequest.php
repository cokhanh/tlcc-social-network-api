<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\UserFriendRequestTransformer;
use Moloquent\Eloquent\SoftDeletes;

class UserFriendRequest extends Moloquent
{
    use SoftDeletes;

    protected $collection = 'user_friend_request';

    protected $guarded = array();

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new UserFriendRequestTransformer();

        return $transformer->transform($this);
    }
}
