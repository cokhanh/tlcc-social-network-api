<?php
return [
    'no-shift'=>'Không có ca làm nào',
    'checked-in'=>'Vào ca thành công',
    'checked-out'=>'Ra ca thành công',
    'success'=>'Thành công',
    'can-not-check-in' => 'Không thể chấm vào ca hai lần',
    'can-not-check-out' => 'Không thể chấm ra ca hai lần'
];