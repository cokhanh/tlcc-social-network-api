<?php

return [
    'incorrect_old_password' => 'Mật khẩu cũ của bạn không chính xác.',
    'change_password_success' => 'Mật khẩu đã được thay đổi thành công.',
    'request_forgot_password' => 'Kiểm tra hộp thư của bạn để lấy đường dẫn thay đổi mật khẩu.',
    'account_not_exist' => 'Tài khoản không tồn tại.',
    'email_exists' => 'Email đăng ký đã tồn tại',
    'female' => 'Nữ',
    'male' => 'Nam',
    'she' => 'cô ấy',
    'he' => 'anh ấy',
    'pls_verify_phone' => 'Vui lòng xác thực số điện thoại',
    'session_time_out' => 'Hết phiên đăng nhập, vui lòng đăng nhập lại!',
];
