<?php

return [
    'success' => 'Thành công.',
    'norecord' => 'Không có dữ liệu',
    'invalid_month' => 'Tháng không hợp lệ',
    'invalid_branch' => 'Chi nhánh không tồn tại',
    'invalid_format' => 'Định dạng không hợp lệ',
    'no_path' => 'Không có đường dẫn tới thư mục',
    'no_status' => 'Bảng tin không tồn tại',
    'invalid_phone' => 'Số điện thoại không hợp lệ',
    'pls_input_image' => 'Vui lòng cung cấp hình ảnh.'
];
