<?php

return [
    'no_request' => 'Không có yêu cầu.',
    'is_friend' => 'Bạn bè.',
    'sent_friend_request' => 'Đã gửi lời mời kết bạn.',
    'pendding_to_accept' => 'Chờ xác nhận.',
    'not_friend' => 'Không phải bạn bè'
];
