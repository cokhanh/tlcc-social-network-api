<?php

return [
    'incorrect' => 'Thông tin đăng nhập không đúng.',
    'unactive' => 'Tài khoản của bạn chưa được kích hoạt.',
    'emptycode' => 'Mã kích hoạt không chính xác hoặc đã hết hạn.',
    'activated' => 'Tài khoản của bạn đã được kích hoạt rồi.',
    'active_require' => 'Kiểm tra hộp thư của bạn để kích hoạt tài khoản.',
    'active_success' => 'Tài khoản của bạn được kích hoạt thành công. Bạn có thể đăng nhập ngay bây giờ.',
    'not_enough_permission' => 'Bạn không đủ quyền để thực hiện hành động này.',
    'invalid_domain' => 'Tên miền không hợp lệ hoặc đã sử dụng.',
    'same_old_password' => 'Mật khẩu mới không được giống mật khẩu cũ',
    'incorrect_old_password' => 'Mật khẩu cũ không chính xác.',
    'invalid_otp' => 'OTP không hợp lệ',
    'password_min_8_max_32' => 'Mật khẩu cần có tối thiếu 8 và tối đa là 32 kí tự.',
    'incorrect_password_confirm' => 'Mật khẩu xác thực không chính xác.'
];
