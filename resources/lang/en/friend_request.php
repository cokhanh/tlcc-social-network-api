<?php

return [
    'no_request' => 'No request.',
    'is_friend' => 'Friend.',
    'sent_friend_request' => 'Sent Invitation.',
    'pendding_to_accept' => 'Accept.',
    'not_friend' => 'Not Friend.'
];
