<?php
return [
    'no-shift'=>'You have no shift today',
    'checked-in'=>'Checked in successfully',
    'checked-out'=>'Checked out successfully',
    'success'=>'Success',
    'can-not-check-in' => 'Can not check in twice',
    'can-not-check-out' => 'Can not check out twice'
];