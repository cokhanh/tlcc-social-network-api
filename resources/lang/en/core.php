<?php

return [
    'success' => 'Success',
    'norecord' => 'No data',
    'invalid_month' => 'Invalid month',
    'invalid_format' => 'Định dạng không hợp lệ',
    'no_path' => 'No such directory in application',
    'no_status' => 'Status does not exist',
    'invalid_phone' => 'Your phone number is not valid',
    'pls_input_image' => 'Please provide your image.'
];
