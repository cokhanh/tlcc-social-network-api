<?php

return [
    'incorrect_old_password' => 'Old password is incorrect',
    'change_password_success' => 'Password changed',
    'request_forgot_password' => 'Check your mail to get link reset password',
    'account_not_exist' => 'Account not exist',
    'email_exists' => 'Email exists, please check again',
    'female' => 'Female',
    'male' => 'Male',
    'she' => 'she',
    'he' => 'he',
    'pls_verify_phone' => 'Verify your phone to continue',
    'session_time_out' => 'Session timeout. Please re-login to continue!',
];
