<?php
return [
    'no_status' => 'Status does not exist',
    'had_updated_avatar' => 'updated avatar of',
    'had_updated_cover' => 'updated cover picture of',
];
