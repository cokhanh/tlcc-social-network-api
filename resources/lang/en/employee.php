<?php
return [
    'no_employee'=>'Employee does not exist',
    'male'=>'Male',
    'female'=>'Female',
    'is-not-staff-of-org'=>'You are not staff of this Organization',
    'no_registered_shift'=>'No shift registered',
    'confirmed' => 'Confirmed',
    'not-confirmed' => 'Is not confirmed'
];