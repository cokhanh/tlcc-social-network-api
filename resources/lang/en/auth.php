<?php
return [
    'incorrect' => 'Email or Rassword is incorrect',
    'unactive' => 'Account has not been activated',
    'emptycode' => 'Can not found CODE',
    'activated' => 'User activated before',
    'active_require' => 'Please check your email to activate account',
    'active_success' => 'Account actived successfully',
    'not_enough_permission' => 'Permission denied',
    'invalid_domain' => 'Domain invalid',
    'same_old_password' => 'Can not be the same as your old password',
    'invalid_otp' => 'OTP is not valid',
    'incorrect_old_password' => 'Old password is incorrect',
    'password_min_8_max_32' => 'Min: 8; Max: 32 character for password.',
    'incorrect_password_confirm' => 'Password to confirm is incorrect.'
];
