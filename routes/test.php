<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->post('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

//v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['cors']], function ($api) {
        $api->get('/', [
            'uses' => 'TestController@test'
        ]);
        $api->post('wifi/address', [
            'uses' => 'TestController@testWifi'
        ]);
        $api->post('/upload', [
            'uses' => 'TestController@store'
        ]);
        $api->post('/image', [
            'uses' => 'TestController@getImage'
        ]);
        $api->get('/get-image', [
            'uses' => 'TestController@loadImage'
        ]);
        $api->post('/key', function () {
            return \Illuminate\Support\Str::random(32);
        });
        $api->post('test/db-connect', [
            'uses' => 'TestController@testDBConnect'
        ]);
        $api->post('test/sms', [
            'uses' => 'TestController@sms'
        ]);
        $api->get('/image/',[
            'uses' => 'TestController@image'
        ]);
        $api->get('/video/',[
            'uses' => 'TestController@video'
        ]);
    });
});
