<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

//v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['cors']], function ($api) {
        $api->post('status/upload-status', [
            'uses' => 'StatusController@uploadStatus'
        ]);
        $api->post('status/update-status', [
            'uses' => 'StatusController@updateStatus'
        ]);
        $api->get('status/load-personal-status', [
            'uses' => 'StatusController@listStatusV1'
        ]);
        $api->post('status/delete', [
            'uses' => 'StatusController@deleteStatus'
        ]);
        $api->get('status/news-feed', [
            'uses' => 'StatusController@listStatusV2'
        ]);
        $api->get('status/get-status', [
            'uses' => 'StatusController@getStatus'
        ]);
    });
});
