<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

//v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['cors']], function ($api) {
        $api->group(['middleware' => ['chat']], function ($api) {
            $api->post('chat/create-chat-group', [
                'uses' => 'ChatController@createChatGroup',
            ]);
        });

        $api->post('chat/save-message', [
            'uses' => 'ChatController@saveMessage'
        ]);
        $api->get('chat/list-messages', [
            'uses' => 'ChatController@listMessages'
        ]);
        $api->get('chat/list-messages-in-group', [
            'uses' => 'ChatController@messagesInGroup'
        ]);
    });
});
